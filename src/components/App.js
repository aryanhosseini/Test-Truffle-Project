import React, { Component } from "react";
import "./App.css";

class App extends Component {
	// Our react code goes in here!
	render() {
		return (
			<div className="app">
				<h1>Hello, World!</h1>
			</div>
		);
	}
}

export default App;
